package com.nexsoft.chatsupportserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatSupportServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatSupportServerApplication.class, args);
    }

}
